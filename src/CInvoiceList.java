import java.util.Locale;
public class CInvoiceList extends CList {
	public CInvoice SearchByNumber(int number) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CInvoice) node.m_Element).m_Number==number) {
				return (CInvoice) node.m_Element;
			}
			node=node.m_Next;
		}
		return null;
	}
	public void PushBack(CInvoice e) {
		super.PushBack(e);
	}
	public boolean ClientHasInvoices(CClient client) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CInvoice) node.m_Element).m_Client==client) {
				return true;
			}
			node=node.m_Next;
		}
		return false;
	}	
	public boolean ProductInInvoices(CProduct product) {
		CNode node=m_Start;
		while (node!=null) {
			CInvoice invoice=(CInvoice) node.m_Element;
			if (invoice.m_Products.MemberP(product)) {
				return true;
			}
			node=node.m_Next;
		}
		return false;
	}
	
	public void printFactures() {
		System.out.println("LISTADO DE FACTURAS MUEBLES JOSE");
		System.out.println("NUMERO DE FACTURA   CLIENTE             IMPORTE");
		CNode node = m_Start;
		while (node != null) {
			CInvoice invoice = (CInvoice) node.m_Element;
			System.out.printf(Locale.ROOT,"%-18d%-20s%.1f\n", invoice.m_Number, invoice.m_Client.m_Name, invoice.m_InvoiceLines.totalLinies());
			node = node.m_Next;
			
		}
		System.out.println("TOTAL: "+totalFactures());

	}
	public float totalFactures() {
		float totalFactures = 0;
		CNode node = m_Start;
		while (node != null) {
			CInvoice invoice = (CInvoice) node.m_Element;
			totalFactures += invoice.m_InvoiceLines.totalLinies();
			node = node.m_Next;
		}
		return totalFactures;

	}
	
}
